from drb.exceptions.core import DrbException, DrbFactoryException


class DrbFtpNodeException(DrbException):
    pass


class DrbFtpNodeFactoryException(DrbFactoryException):
    pass
