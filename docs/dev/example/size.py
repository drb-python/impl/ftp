from requests.auth import HTTPBasicAuth

from drb.drivers.ftp import DrbFtpNode

node = DrbFtpNode("YOUR_URL_SERVER", host="YOUR_HOST", auth=HTTPBasicAuth("USER", "PWD"))

# Get the size of the file file1.txt
node['file.txt'] @ 'size'
