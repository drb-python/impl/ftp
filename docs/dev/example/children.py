from requests.auth import HTTPBasicAuth

from drb.drivers.ftp import DrbFtpNode

node = DrbFtpNode("YOUR_URL_SERVER", host="YOUR_HOST", auth=HTTPBasicAuth("USER", "PWD"))

# print(node.name)
if node.has_child():
    for e in node:
        # Do Something with your children
        print(e.name)
