import io

from requests.auth import HTTPBasicAuth

from drb.drivers.ftp import DrbFtpNode

node = DrbFtpNode("YOUR_URL_SERVER", host="YOUR_HOST", auth=HTTPBasicAuth("USER", "PWD"))

# Download all the file
with node['file.txt'].get_impl(io.BytesIO) as stream:
    stream.read().decode()

# Download only the five first byte of the file
with node['file.txt'].get_impl(io.BytesIO) as stream:
    stream.read(5).decode()
