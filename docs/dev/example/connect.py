from requests.auth import HTTPBasicAuth

from drb.drivers.ftp import DrbFtpNode
from ftplib import FTP_TLS

# To connect to a simple FTP server
node = DrbFtpNode("YOUR_URL_SERVER", host="YOUR_HOST", auth=HTTPBasicAuth("USER", "PWD"))

# to connect to a server using TLS protocol PROTOCOL_TLS
node = DrbFtpNode("YOUR_URL_SERVER", host="YOUR_HOST", auth=HTTPBasicAuth("USER", "PWD"), protocol=FTP_TLS)
