from requests.auth import HTTPBasicAuth

from drb.drivers.ftp import DrbFtpNode

node = DrbFtpNode("YOUR_URL_SERVER", host="YOUR_HOST", auth=HTTPBasicAuth("USER", "PWD"))

# Return true if the file is a directory False otherwise
node['file.txt'] @ 'directory'