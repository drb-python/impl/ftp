.. _install:

Installation of ftp driver
==================================
Installing ``drb-driver-ftp`` with execute the following in a terminal:

.. code-block::

   pip install drb-driver-ftp
