===================
Data Request Broker
===================
---------------------------------
FTP driver for DRB
---------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-drive-ftp/month
    :target: https://pepy.tech/project/drb-drive-ftp
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-drive-ftp.svg
    :target: https://pypi.org/project/drb-drive-ftp/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-drive-ftp.svg
    :target: https://pypi.org/project/drb-drive-ftp/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-drive-ftp.svg
    :target: https://pypi.org/project/drb-drive-ftp/
    :alt: Python Version Support Badge

-------------------

This python module includes data model driver of FTP protocol

User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example

Others
======
.. toctree::
   :maxdepth: 2

   user/limitation
